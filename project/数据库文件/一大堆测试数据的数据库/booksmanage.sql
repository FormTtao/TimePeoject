/*
Navicat MySQL Data Transfer

Source Server         : 阿里云
Source Server Version : 50722
Source Host           : 120.78.75.213:3306
Source Database       : booksmanage

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2018-09-20 09:16:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `b_art`
-- ----------------------------
DROP TABLE IF EXISTS `b_art`;
CREATE TABLE `b_art` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book4.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '4',
  `remove` int(2) DEFAULT '0' COMMENT '0代表false，1代表true',
  PRIMARY KEY (`id`),
  KEY `艺术与摄影` (`kindid`),
  CONSTRAINT `艺术与摄影` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_art
-- ----------------------------
INSERT INTO `b_art` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('2', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('3', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('4', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('58', '斗破苍穹58', '2018-07-25 09:59:06', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'books', '4', '0');
INSERT INTO `b_art` VALUES ('60', '达到ad', '2018-07-25 16:21:55', 'as打算', 'http://120.78.75.213:8080/books/uploads/img/1532507121013免责声明8.txt', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', 'undefined', '4', '0');
INSERT INTO `b_art` VALUES ('61', '11', '1', '1', '1', '1', 'http://120.78.75.213:8080/kindid/book4.jpg', null, '4', '0');

-- ----------------------------
-- Table structure for `b_biography`
-- ----------------------------
DROP TABLE IF EXISTS `b_biography`;
CREATE TABLE `b_biography` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book2.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '2',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `传记` (`kindid`),
  CONSTRAINT `传记` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_biography
-- ----------------------------
INSERT INTO `b_biography` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('55', '斗破苍穹55', '2018-07-25 15:52:40', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('56', '斗破苍穹56', '2018-07-25 15:55:28', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('57', '斗破苍穹57', '2018-08-06 14:20:03', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('58', '斗破苍穹58', '2018-07-25 15:28:13', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('59', '斗破苍穹59', '2018-07-25 15:27:12', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'books', '2', '0');
INSERT INTO `b_biography` VALUES ('60', '顶顶顶顶', '2018-07-25 16:20:58', '撒大声地', 'http://120.78.75.213:8080/books/uploads/img/1532506939923免责声明7.txt', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'undefined', '2', '0');
INSERT INTO `b_biography` VALUES ('61', '大武当无', '2018-07-26 14:10:43', '撒大声地', 'http://120.78.75.213:8080/books/uploads/img/1532506939923免责声明7.txt', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'undefined', '2', '0');
INSERT INTO `b_biography` VALUES ('62', '大娃娃', '2018-07-26 14:15:50', 'www大多所多是的asdasd', 'http://120.78.75.213:8080/books/uploads/img/1532585954187免责声明13.txt', '1', 'http://120.78.75.213:8080/kindid/book2.jpg', 'undefined', '2', '0');

-- ----------------------------
-- Table structure for `b_cartoon`
-- ----------------------------
DROP TABLE IF EXISTS `b_cartoon`;
CREATE TABLE `b_cartoon` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book3.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '3',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `青春动漫` (`kindid`),
  CONSTRAINT `青春动漫` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_cartoon
-- ----------------------------
INSERT INTO `b_cartoon` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');
INSERT INTO `b_cartoon` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book3.jpg', 'books', '3', '0');

-- ----------------------------
-- Table structure for `b_children`
-- ----------------------------
DROP TABLE IF EXISTS `b_children`;
CREATE TABLE `b_children` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book5.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '5',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `儿童` (`kindid`),
  CONSTRAINT `儿童` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_children
-- ----------------------------
INSERT INTO `b_children` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');
INSERT INTO `b_children` VALUES ('60', '斗破苍11111', '2018-07-25 10:43:23', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book5.jpg', 'books', '5', '0');

-- ----------------------------
-- Table structure for `b_fun`
-- ----------------------------
DROP TABLE IF EXISTS `b_fun`;
CREATE TABLE `b_fun` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book9.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '9',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `娱乐与体育` (`kindid`),
  CONSTRAINT `娱乐与体育` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_fun
-- ----------------------------
INSERT INTO `b_fun` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');
INSERT INTO `b_fun` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book9.jpg', 'books', '9', '0');

-- ----------------------------
-- Table structure for `b_history`
-- ----------------------------
DROP TABLE IF EXISTS `b_history`;
CREATE TABLE `b_history` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book7.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '7',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `历史` (`kindid`),
  CONSTRAINT `历史` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_history
-- ----------------------------
INSERT INTO `b_history` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');
INSERT INTO `b_history` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book7.jpg', 'books', '7', '0');

-- ----------------------------
-- Table structure for `b_language`
-- ----------------------------
DROP TABLE IF EXISTS `b_language`;
CREATE TABLE `b_language` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book12.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '12',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `外语学习` (`kindid`),
  CONSTRAINT `外语学习` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_language
-- ----------------------------
INSERT INTO `b_language` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');
INSERT INTO `b_language` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book12.jpg', 'books', '12', '0');

-- ----------------------------
-- Table structure for `b_life`
-- ----------------------------
DROP TABLE IF EXISTS `b_life`;
CREATE TABLE `b_life` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book11.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '11',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `科技与生活` (`kindid`),
  CONSTRAINT `科技与生活` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_life
-- ----------------------------
INSERT INTO `b_life` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');
INSERT INTO `b_life` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book11.jpg', 'books', '11', '0');

-- ----------------------------
-- Table structure for `b_literature`
-- ----------------------------
DROP TABLE IF EXISTS `b_literature`;
CREATE TABLE `b_literature` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book1.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '1',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `文学` (`kindid`),
  CONSTRAINT `文学` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_literature
-- ----------------------------
INSERT INTO `b_literature` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('58', '斗破', '2018-07-25 10:42:44', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('59', '斗破苍穹59', '2018-07-25 15:40:21', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('60', '等等', '2018-07-25 16:08:33', '撒打算', 'http://120.78.75.213:8080/books/uploads/img/1532506319018免责声明2.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('61', '湿哒哒所是打算', '2018-07-25 16:13:24', 'www多多多多', 'http://120.78.75.213:8080/books/uploads/img/1532506608183免责声明4.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('62', '啥的伟大的是的as', '2018-07-25 16:16:50', '撒大声地asd', 'http://120.78.75.213:8080/books/uploads/img/1532506815915免责声明6.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('64', '你好', '2018-07-26 14:12:52', '你还会', 'http://120.78.75.213:8080/books/uploads/img/1533536360516免责声明1.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('65', 'test1', '2018-08-06 09:21:56', '嘻嘻嘻嘻，动一动一', 'http://120.78.75.213:8080/books/uploads/img/1533518508859免责声明1.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('66', 'test1', '2018-08-06 10:11:46', '嘻嘻嘻is好的骄傲是', 'http://120.78.75.213:8080/books/uploads/img/1533521502125免责声明.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('67', 'test1', '2018-08-06 14:19:22', 'aaaaa', 'http://120.78.75.213:8080/books/uploads/img/1533536360516免责声明1.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('68', 'test1', '2018-08-06 14:19:22', '2018-08-06 14:19:22', 'http://120.78.75.213:8080/books/uploads/img/1533536360516免责声明1.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');
INSERT INTO `b_literature` VALUES ('69', '1111', '2018-08-08 10:32:41', '1111', 'http://120.78.75.213:8080/books/uploads/ebooks/1533781183353胡.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('70', '是的撒', '2018-08-08 10:39:05', '达大厦', 'http://120.78.75.213:8080/books/uploads/ebooks/1533781222976胡1.txt', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('71', '12131', '2018-08-08 11:13:05', '121321321', 'http://120.78.75.213:8080/kindid/book3.jpg', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('72', '2222', '2018-08-08 11:19:17', '的撒打算的', 'http://120.78.75.213:8080/kindid/book2.jpg', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'undefined', '1', '0');
INSERT INTO `b_literature` VALUES ('73', 'ssssss', '2018-08-08 11:53:28', 'sssss41s4d5as4d54as654d65as4d654as564d65a4s65d4as654d654asd654as654d65as4d654a65s4d654as654d65as4d54as65d465as4d654as654d65as4d56as4d54as5d4as654d654as654d65as4d654as654d65a4s56d465a4s65d465as4d654as65d465as4d654as65d4as65', 'http://120.78.75.213:8080/kindid/book1.jpg', '1', 'http://120.78.75.213:8080/kindid/book1.jpg', 'books', '1', '0');

-- ----------------------------
-- Table structure for `b_novel`
-- ----------------------------
DROP TABLE IF EXISTS `b_novel`;
CREATE TABLE `b_novel` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book13.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '13',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `novel` (`kindid`),
  CONSTRAINT `novel` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_novel
-- ----------------------------
INSERT INTO `b_novel` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', 'books', '13', '0');
INSERT INTO `b_novel` VALUES ('60', 'as大四的', '2018-07-25 16:10:31', 'as打算打算大四的as 是', 'http://120.78.75.213:8080/books/uploads/img/1532506435205免责声明3.txt', '1', 'http://120.78.75.213:8080/kindid/book13.jpg', '是金合欢花', '13', '0');

-- ----------------------------
-- Table structure for `b_social`
-- ----------------------------
DROP TABLE IF EXISTS `b_social`;
CREATE TABLE `b_social` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book6.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '6',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `社会与科学` (`kindid`),
  CONSTRAINT `社会与科学` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_social
-- ----------------------------
INSERT INTO `b_social` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');
INSERT INTO `b_social` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book6.jpg', 'books', '6', '0');

-- ----------------------------
-- Table structure for `b_succer`
-- ----------------------------
DROP TABLE IF EXISTS `b_succer`;
CREATE TABLE `b_succer` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book8.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '8',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `哲学与成功` (`kindid`),
  CONSTRAINT `哲学与成功` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_succer
-- ----------------------------
INSERT INTO `b_succer` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');
INSERT INTO `b_succer` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book8.jpg', 'books', '8', '0');

-- ----------------------------
-- Table structure for `b_traval`
-- ----------------------------
DROP TABLE IF EXISTS `b_traval`;
CREATE TABLE `b_traval` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT 'http://120.78.75.213:8080/kindid/book10.jpg',
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '10',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `旅游` (`kindid`),
  CONSTRAINT `旅游` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_traval
-- ----------------------------
INSERT INTO `b_traval` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');
INSERT INTO `b_traval` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/kindid/book10.jpg', 'books', '10', '0');

-- ----------------------------
-- Table structure for `collcetion_book`
-- ----------------------------
DROP TABLE IF EXISTS `collcetion_book`;
CREATE TABLE `collcetion_book` (
  `collcetion_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '收藏书籍的ID',
  `collcetion_book_id` int(11) NOT NULL COMMENT '收藏书籍的ID',
  `collcetion_book_kindid` int(11) NOT NULL COMMENT '收藏书籍的种类ID',
  `collcetion_username` varchar(255) DEFAULT NULL COMMENT '收藏的用户名',
  `collcetion_userId` int(11) NOT NULL COMMENT '收藏用户的id',
  PRIMARY KEY (`collcetion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of collcetion_book
-- ----------------------------
INSERT INTO `collcetion_book` VALUES ('1', '73', '1', 'juntao', '1');
INSERT INTO `collcetion_book` VALUES ('2', '73', '1', 'juntao', '1');
INSERT INTO `collcetion_book` VALUES ('13', '73', '1', 'lin', '2');
INSERT INTO `collcetion_book` VALUES ('14', '73', '1', 'lin', '2');

-- ----------------------------
-- Table structure for `g_user`
-- ----------------------------
DROP TABLE IF EXISTS `g_user`;
CREATE TABLE `g_user` (
  `date` varchar(255) DEFAULT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是管理员的表格',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL COMMENT '这是管理员的表格',
  `nul` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of g_user
-- ----------------------------
INSERT INTO `g_user` VALUES ('2018-07-11 09:06:50', '1', 'books', '123456', '管理员1', '0');
INSERT INTO `g_user` VALUES ('2018-07-11 09:07:00', '2', 'trues', '123456', '管理员2', '0');
INSERT INTO `g_user` VALUES ('2018-07-11 19:23:40', '3', '1245', '123456', '撒大声对对对 ', '0');

-- ----------------------------
-- Table structure for `kindid`
-- ----------------------------
DROP TABLE IF EXISTS `kindid`;
CREATE TABLE `kindid` (
  `kindid` int(255) NOT NULL AUTO_INCREMENT,
  `kindname` varchar(255) DEFAULT NULL COMMENT '这是分类的表格',
  PRIMARY KEY (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kindid
-- ----------------------------
INSERT INTO `kindid` VALUES ('1', '文学');
INSERT INTO `kindid` VALUES ('2', '传记');
INSERT INTO `kindid` VALUES ('3', '青春动漫');
INSERT INTO `kindid` VALUES ('4', '艺术与摄影');
INSERT INTO `kindid` VALUES ('5', '少儿');
INSERT INTO `kindid` VALUES ('6', '社会科学');
INSERT INTO `kindid` VALUES ('7', '历史');
INSERT INTO `kindid` VALUES ('8', '励志与成功');
INSERT INTO `kindid` VALUES ('9', '娱乐与体育');
INSERT INTO `kindid` VALUES ('10', '旅游');
INSERT INTO `kindid` VALUES ('11', '科技与生活');
INSERT INTO `kindid` VALUES ('12', '外语学习');
INSERT INTO `kindid` VALUES ('13', '小说');

-- ----------------------------
-- Table structure for `mlog`
-- ----------------------------
DROP TABLE IF EXISTS `mlog`;
CREATE TABLE `mlog` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `loguser` varchar(255) DEFAULT NULL,
  `logtime` varchar(255) DEFAULT NULL,
  `logcontent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mlog
-- ----------------------------
INSERT INTO `mlog` VALUES ('3', 'books', '2018-07-05 17:25:56', '[普通用户:books]在2018-07-05 17:25:56,操作了:/login方法');
INSERT INTO `mlog` VALUES ('4', 'books', '2018-07-05 17:25:56', '[普通用户:books]在2018-07-05 17:25:56,操作了:/showManager方法');
INSERT INTO `mlog` VALUES ('5', 'books', '2018-07-05 17:30:03', '[管理员: books]在 2018-07-05 17:30:03,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('6', 'books', '2018-07-05 17:30:03', '[管理员: books]在 2018-07-05 17:30:03,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('7', 'books', '2018-07-05 17:30:11', '[管理员: books]在 2018-07-05 17:30:11,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('8', 'books', '2018-07-09 15:02:03', '[管理员: books]在 2018-07-09 15:02:03,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('9', 'books', '2018-07-09 15:02:03', '[管理员: books]在 2018-07-09 15:02:03,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('10', 'books', '2018-07-09 15:02:15', '[管理员: books]在 2018-07-09 15:02:15,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('11', 'books', '2018-07-09 15:02:16', '[管理员: books]在 2018-07-09 15:02:16,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('12', 'books', '2018-07-09 15:02:16', '[管理员: books]在 2018-07-09 15:02:16,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('13', 'books', '2018-07-09 15:02:17', '[管理员: books]在 2018-07-09 15:02:17,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('14', 'books', '2018-07-09 15:02:18', '[管理员: books]在 2018-07-09 15:02:18,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('15', 'books', '2018-07-09 15:02:18', '[管理员: books]在 2018-07-09 15:02:18,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('16', 'books', '2018-07-09 15:04:15', '[管理员: books]在 2018-07-09 15:04:15,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('17', 'books', '2018-07-09 15:04:58', '[管理员: books]在 2018-07-09 15:04:58,操作了: /showManagerLog方法 .');
INSERT INTO `mlog` VALUES ('18', 'books', '2018-07-09 15:10:54', '[管理员: books]在 2018-07-09 15:10:54,操作了: /showManagerLog方法 .');
INSERT INTO `mlog` VALUES ('19', 'books', '2018-07-24 16:06:53', '[管理员: books]在 2018-07-24 16:06:53,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('20', 'books', '2018-07-24 16:06:53', '[管理员: books]在 2018-07-24 16:06:53,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('21', 'books', '2018-07-27 15:13:45', '[管理员: books]在 2018-07-27 15:13:45,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('22', 'books', '2018-07-27 15:13:45', '[管理员: books]在 2018-07-27 15:13:45,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('23', 'books', '2018-07-27 15:17:49', '[管理员: books]在 2018-07-27 15:17:49,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('24', 'books', '2018-07-27 15:17:49', '[管理员: books]在 2018-07-27 15:17:49,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('25', 'books', '2018-07-27 15:23:28', '[管理员: books]在 2018-07-27 15:23:28,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('26', 'books', '2018-07-27 15:23:33', '[管理员: books]在 2018-07-27 15:23:33,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('27', 'books', '2018-07-27 15:23:51', '[管理员: books]在 2018-07-27 15:23:51,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('28', 'books', '2018-07-27 15:23:51', '[管理员: books]在 2018-07-27 15:23:51,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('29', 'books', '2018-07-27 15:24:08', '[管理员: books]在 2018-07-27 15:24:08,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('30', 'books', '2018-07-27 15:24:08', '[管理员: books]在 2018-07-27 15:24:08,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('31', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('32', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('33', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('34', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('35', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('36', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('37', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('38', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('39', 'books', '2018-07-27 15:24:13', '[管理员: books]在 2018-07-27 15:24:13,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('40', 'books', '2018-07-27 15:24:14', '[管理员: books]在 2018-07-27 15:24:14,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('41', 'books', '2018-07-27 15:24:14', '[管理员: books]在 2018-07-27 15:24:14,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('42', 'books', '2018-07-27 15:24:14', '[管理员: books]在 2018-07-27 15:24:14,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('43', 'books', '2018-07-27 15:24:14', '[管理员: books]在 2018-07-27 15:24:14,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('44', 'books', '2018-07-27 15:37:53', '[管理员: books]在 2018-07-27 15:37:53,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('45', 'books', '2018-07-27 15:37:53', '[管理员: books]在 2018-07-27 15:37:53,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('46', 'books', '2018-07-27 15:38:05', '[管理员: books]在 2018-07-27 15:38:05,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('47', 'books', '2018-07-27 15:38:05', '[管理员: books]在 2018-07-27 15:38:05,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('48', 'books', '2018-07-27 15:38:09', '[管理员: books]在 2018-07-27 15:38:09,操作了: /newData方法 .');
INSERT INTO `mlog` VALUES ('49', 'books', '2018-07-31 15:49:06', '[管理员: books]在 2018-07-31 15:49:06,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('50', 'books', '2018-07-31 15:49:06', '[管理员: books]在 2018-07-31 15:49:06,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('51', 'books', '2018-07-31 15:49:22', '[管理员: books]在 2018-07-31 15:49:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('52', 'books', '2018-07-31 15:49:22', '[管理员: books]在 2018-07-31 15:49:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('53', 'books', '2018-07-31 15:49:22', '[管理员: books]在 2018-07-31 15:49:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('54', 'books', '2018-07-31 15:49:22', '[管理员: books]在 2018-07-31 15:49:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('55', 'books', '2018-07-31 15:49:22', '[管理员: books]在 2018-07-31 15:49:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('56', 'books', '2018-07-31 15:49:22', '[管理员: books]在 2018-07-31 15:49:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('57', 'books', '2018-07-31 15:49:22', '[管理员: books]在 2018-07-31 15:49:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('58', 'books', '2018-07-31 15:49:22', '[管理员: books]在 2018-07-31 15:49:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('59', 'books', '2018-07-31 15:49:23', '[管理员: books]在 2018-07-31 15:49:23,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('60', 'books', '2018-07-31 15:49:23', '[管理员: books]在 2018-07-31 15:49:23,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('61', 'books', '2018-07-31 15:49:23', '[管理员: books]在 2018-07-31 15:49:23,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('62', 'books', '2018-07-31 15:49:23', '[管理员: books]在 2018-07-31 15:49:23,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('63', 'books', '2018-07-31 15:49:23', '[管理员: books]在 2018-07-31 15:49:23,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('64', 'books', '2018-07-31 15:49:43', '[管理员: books]在 2018-07-31 15:49:43,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('65', 'books', '2018-07-31 15:49:43', '[管理员: books]在 2018-07-31 15:49:43,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('66', 'books', '2018-07-31 15:49:48', '[管理员: books]在 2018-07-31 15:49:48,操作了: /newData方法 .');
INSERT INTO `mlog` VALUES ('67', 'books', '2018-07-31 15:49:57', '[管理员: books]在 2018-07-31 15:49:57,操作了: /showManagerLog方法 .');
INSERT INTO `mlog` VALUES ('68', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('69', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('70', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('71', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('72', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('73', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('74', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('75', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('76', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('77', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('78', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('79', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('80', 'books', '2018-07-31 15:50:53', '[管理员: books]在 2018-07-31 15:50:53,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('81', 'books', '2018-07-31 15:51:02', '[管理员: books]在 2018-07-31 15:51:02,操作了: /showManagerLog方法 .');
INSERT INTO `mlog` VALUES ('82', 'books', '2018-07-31 15:51:11', '[管理员: books]在 2018-07-31 15:51:11,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('83', 'books', '2018-07-31 15:51:11', '[管理员: books]在 2018-07-31 15:51:11,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('84', 'books', '2018-07-31 15:51:17', '[管理员: books]在 2018-07-31 15:51:17,操作了: /showManagerLog方法 .');
INSERT INTO `mlog` VALUES ('85', 'books', '2018-07-31 15:51:23', '[管理员: books]在 2018-07-31 15:51:23,操作了: /showUserLog方法 .');
INSERT INTO `mlog` VALUES ('86', 'books', '2018-08-01 11:04:34', '[管理员: books]在 2018-08-01 11:04:34,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('87', 'books', '2018-08-01 11:04:34', '[管理员: books]在 2018-08-01 11:04:34,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('88', 'books', '2018-08-01 11:04:52', '[管理员: books]在 2018-08-01 11:04:52,操作了: /showManagerLog方法 .');
INSERT INTO `mlog` VALUES ('89', 'books', '2018-08-01 11:25:14', '[管理员: books]在 2018-08-01 11:25:14,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('90', 'books', '2018-08-01 11:25:14', '[管理员: books]在 2018-08-01 11:25:14,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('91', 'books', '2018-08-01 11:25:31', '[管理员: books]在 2018-08-01 11:25:31,操作了: /allNews方法 .');
INSERT INTO `mlog` VALUES ('92', 'books', '2018-08-01 11:25:58', '[管理员: books]在 2018-08-01 11:25:58,操作了: /showUserLog方法 .');
INSERT INTO `mlog` VALUES ('93', 'books', '2018-08-01 11:26:08', '[管理员: books]在 2018-08-01 11:26:08,操作了: /showUserLog方法 .');
INSERT INTO `mlog` VALUES ('94', 'books', '2018-08-01 11:26:18', '[管理员: books]在 2018-08-01 11:26:18,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('95', 'books', '2018-08-01 11:26:18', '[管理员: books]在 2018-08-01 11:26:18,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('96', 'books', '2018-08-01 11:26:31', '[管理员: books]在 2018-08-01 11:26:31,操作了: /exit方法 .');
INSERT INTO `mlog` VALUES ('97', 'books', '2018-08-01 20:05:35', '[管理员: books]在 2018-08-01 20:05:35,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('98', 'books', '2018-08-01 20:05:36', '[管理员: books]在 2018-08-01 20:05:36,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('99', 'books', '2018-08-01 20:06:02', '[管理员: books]在 2018-08-01 20:06:02,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('100', 'books', '2018-08-01 20:06:02', '[管理员: books]在 2018-08-01 20:06:02,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('101', 'books', '2018-08-01 20:06:08', '[管理员: books]在 2018-08-01 20:06:08,操作了: /findPeopleUser方法 .');
INSERT INTO `mlog` VALUES ('102', 'books', '2018-08-01 20:06:10', '[管理员: books]在 2018-08-01 20:06:10,操作了: /findPeopleUser方法 .');
INSERT INTO `mlog` VALUES ('103', 'books', '2018-08-02 15:54:46', '[管理员: books]在 2018-08-02 15:54:46,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('104', 'books', '2018-08-02 15:54:47', '[管理员: books]在 2018-08-02 15:54:47,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('105', 'books', '2018-08-02 16:05:08', '[管理员: books]在 2018-08-02 16:05:08,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('106', 'books', '2018-08-02 16:05:08', '[管理员: books]在 2018-08-02 16:05:08,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('107', 'books', '2018-08-03 10:50:07', '[管理员: books]在 2018-08-03 10:50:07,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('108', 'books', '2018-08-03 10:50:07', '[管理员: books]在 2018-08-03 10:50:07,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('109', 'books', '2018-08-03 10:50:25', '[管理员: books]在 2018-08-03 10:50:25,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('110', 'books', '2018-08-03 10:50:25', '[管理员: books]在 2018-08-03 10:50:25,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('111', 'books', '2018-08-03 22:19:42', '[管理员: books]在 2018-08-03 22:19:42,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('112', 'books', '2018-08-03 22:19:42', '[管理员: books]在 2018-08-03 22:19:42,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('113', 'books', '2018-08-03 22:19:49', '[管理员: books]在 2018-08-03 22:19:49,操作了: /exit方法 .');
INSERT INTO `mlog` VALUES ('114', 'books', '2018-08-03 23:07:17', '[管理员: books]在 2018-08-03 23:07:17,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('115', 'books', '2018-08-03 23:07:17', '[管理员: books]在 2018-08-03 23:07:17,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('116', 'books', '2018-08-03 23:07:38', '[管理员: books]在 2018-08-03 23:07:38,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('117', 'books', '2018-08-03 23:07:38', '[管理员: books]在 2018-08-03 23:07:38,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('118', 'books', '2018-08-04 09:54:50', '[管理员: books]在 2018-08-04 09:54:50,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('119', 'books', '2018-08-04 09:54:50', '[管理员: books]在 2018-08-04 09:54:50,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('120', 'books', '2018-08-04 09:55:08', '[管理员: books]在 2018-08-04 09:55:08,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('121', 'books', '2018-08-04 09:55:13', '[管理员: books]在 2018-08-04 09:55:13,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('122', 'books', '2018-08-04 09:55:13', '[管理员: books]在 2018-08-04 09:55:13,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('123', 'books', '2018-08-04 10:00:16', '[管理员: books]在 2018-08-04 10:00:16,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('124', 'books', '2018-08-04 10:00:16', '[管理员: books]在 2018-08-04 10:00:16,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('125', 'books', '2018-08-04 10:00:21', '[管理员: books]在 2018-08-04 10:00:21,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('126', 'books', '2018-08-04 10:00:21', '[管理员: books]在 2018-08-04 10:00:21,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('127', 'books', '2018-08-04 12:51:48', '[管理员: books]在 2018-08-04 12:51:48,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('128', 'books', '2018-08-04 12:51:48', '[管理员: books]在 2018-08-04 12:51:48,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('129', 'books', '2018-08-04 12:52:02', '[管理员: books]在 2018-08-04 12:52:02,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('130', 'books', '2018-08-06 09:12:13', '[管理员: books]在 2018-08-06 09:12:13,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('131', 'books', '2018-08-06 09:12:13', '[管理员: books]在 2018-08-06 09:12:13,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('132', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('133', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('134', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('135', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('136', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('137', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('138', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('139', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('140', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('141', 'books', '2018-08-06 09:12:29', '[管理员: books]在 2018-08-06 09:12:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('142', 'books', '2018-08-06 09:12:30', '[管理员: books]在 2018-08-06 09:12:30,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('143', 'books', '2018-08-06 09:12:30', '[管理员: books]在 2018-08-06 09:12:30,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('144', 'books', '2018-08-06 09:12:30', '[管理员: books]在 2018-08-06 09:12:30,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('145', 'books', '2018-08-06 10:06:21', '[管理员: books]在 2018-08-06 10:06:21,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('146', 'books', '2018-08-06 10:06:21', '[管理员: books]在 2018-08-06 10:06:21,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('147', 'books', '2018-08-06 10:06:25', '[管理员: books]在 2018-08-06 10:06:25,操作了: /allNews方法 .');
INSERT INTO `mlog` VALUES ('148', 'books', '2018-08-06 10:06:28', '[管理员: books]在 2018-08-06 10:06:28,操作了: /newData方法 .');
INSERT INTO `mlog` VALUES ('149', 'books', '2018-08-06 10:06:52', '[管理员: books]在 2018-08-06 10:06:52,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('150', 'books', '2018-08-06 10:06:52', '[管理员: books]在 2018-08-06 10:06:52,操作了: /newData方法 .');
INSERT INTO `mlog` VALUES ('151', 'books', '2018-08-06 10:09:11', '[管理员: books]在 2018-08-06 10:09:11,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('152', 'books', '2018-08-06 10:09:12', '[管理员: books]在 2018-08-06 10:09:12,操作了: /newData方法 .');
INSERT INTO `mlog` VALUES ('153', 'books', '2018-08-06 10:11:09', '[管理员: books]在 2018-08-06 10:11:09,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('154', 'books', '2018-08-06 10:11:09', '[管理员: books]在 2018-08-06 10:11:09,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('155', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('156', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('157', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('158', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('159', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('160', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('161', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('162', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('163', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('164', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('165', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('166', 'books', '2018-08-06 10:11:15', '[管理员: books]在 2018-08-06 10:11:15,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('167', 'books', '2018-08-06 10:11:16', '[管理员: books]在 2018-08-06 10:11:16,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('168', 'books', '2018-08-06 10:11:46', '[管理员: books]在 2018-08-06 10:11:46,操作了: /addData方法 .');
INSERT INTO `mlog` VALUES ('169', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('170', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('171', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('172', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('173', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('174', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('175', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('176', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('177', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('178', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('179', 'books', '2018-08-06 10:11:51', '[管理员: books]在 2018-08-06 10:11:51,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('180', 'books', '2018-08-06 10:11:52', '[管理员: books]在 2018-08-06 10:11:52,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('181', 'books', '2018-08-06 10:11:52', '[管理员: books]在 2018-08-06 10:11:52,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('182', 'books', '2018-08-06 14:12:35', '[管理员: books]在 2018-08-06 14:12:35,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('183', 'books', '2018-08-06 14:12:35', '[管理员: books]在 2018-08-06 14:12:35,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('184', 'books', '2018-08-06 14:12:40', '[管理员: books]在 2018-08-06 14:12:40,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('185', 'books', '2018-08-06 14:12:40', '[管理员: books]在 2018-08-06 14:12:40,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('186', 'books', '2018-08-06 14:12:45', '[管理员: books]在 2018-08-06 14:12:45,操作了: /upDataUsers方法 .');
INSERT INTO `mlog` VALUES ('187', 'books', '2018-08-06 14:12:45', '[管理员: books]在 2018-08-06 14:12:45,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('188', 'books', '2018-08-06 14:12:45', '[管理员: books]在 2018-08-06 14:12:45,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('189', 'books', '2018-08-06 14:12:46', '[管理员: books]在 2018-08-06 14:12:46,操作了: /userPeople方法 .');
INSERT INTO `mlog` VALUES ('190', 'books', '2018-08-06 14:12:46', '[管理员: books]在 2018-08-06 14:12:46,操作了: /managerPeople方法 .');
INSERT INTO `mlog` VALUES ('191', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('192', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('193', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('194', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('195', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('196', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('197', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('198', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('199', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('200', 'books', '2018-08-06 14:18:21', '[管理员: books]在 2018-08-06 14:18:21,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('201', 'books', '2018-08-06 14:18:22', '[管理员: books]在 2018-08-06 14:18:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('202', 'books', '2018-08-06 14:18:22', '[管理员: books]在 2018-08-06 14:18:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('203', 'books', '2018-08-06 14:18:22', '[管理员: books]在 2018-08-06 14:18:22,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('204', 'books', '2018-08-06 14:19:22', '[管理员: books]在 2018-08-06 14:19:22,操作了: /addData方法 .');
INSERT INTO `mlog` VALUES ('205', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('206', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('207', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('208', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('209', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('210', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('211', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('212', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('213', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('214', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('215', 'books', '2018-08-06 14:19:28', '[管理员: books]在 2018-08-06 14:19:28,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('216', 'books', '2018-08-06 14:19:29', '[管理员: books]在 2018-08-06 14:19:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('217', 'books', '2018-08-06 14:19:29', '[管理员: books]在 2018-08-06 14:19:29,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('218', 'books', '2018-08-06 14:20:03', '[管理员: books]在 2018-08-06 14:20:03,操作了: /upData方法 .');
INSERT INTO `mlog` VALUES ('219', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('220', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('221', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('222', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('223', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('224', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('225', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('226', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('227', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('228', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('229', 'books', '2018-08-06 14:20:08', '[管理员: books]在 2018-08-06 14:20:08,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('230', 'books', '2018-08-06 14:20:09', '[管理员: books]在 2018-08-06 14:20:09,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('231', 'books', '2018-08-06 14:20:09', '[管理员: books]在 2018-08-06 14:20:09,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('232', 'books', '2018-08-07 10:28:27', '[管理员: books]在 2018-08-07 10:28:27,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('233', 'books', '2018-08-07 10:28:27', '[管理员: books]在 2018-08-07 10:28:27,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('234', 'books', '2018-08-07 10:28:39', '[管理员: books]在 2018-08-07 10:28:39,操作了: /allNews方法 .');
INSERT INTO `mlog` VALUES ('235', 'books', '2018-08-07 10:28:59', '[管理员: books]在 2018-08-07 10:28:59,操作了: /addNews方法 .');
INSERT INTO `mlog` VALUES ('236', 'books', '2018-08-07 10:28:59', '[管理员: books]在 2018-08-07 10:28:59,操作了: /allNews方法 .');
INSERT INTO `mlog` VALUES ('237', 'books', '2018-08-07 10:29:00', '[管理员: books]在 2018-08-07 10:29:00,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('238', 'books', '2018-08-07 10:29:01', '[管理员: books]在 2018-08-07 10:29:01,操作了: /allNews方法 .');
INSERT INTO `mlog` VALUES ('239', 'books', '2018-08-08 11:50:32', '[管理员: books]在 2018-08-08 11:50:32,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('240', 'books', '2018-08-08 11:50:32', '[管理员: books]在 2018-08-08 11:50:32,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('241', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('242', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('243', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('244', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('245', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('246', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('247', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('248', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('249', 'books', '2018-08-08 11:50:45', '[管理员: books]在 2018-08-08 11:50:45,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('250', 'books', '2018-08-08 11:50:46', '[管理员: books]在 2018-08-08 11:50:46,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('251', 'books', '2018-08-08 11:50:46', '[管理员: books]在 2018-08-08 11:50:46,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('252', 'books', '2018-08-08 11:50:46', '[管理员: books]在 2018-08-08 11:50:46,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('253', 'books', '2018-08-08 11:50:46', '[管理员: books]在 2018-08-08 11:50:46,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('254', 'books', '2018-08-08 11:52:50', '[管理员: books]在 2018-08-08 11:52:50,操作了: /login方法 .');
INSERT INTO `mlog` VALUES ('255', 'books', '2018-08-08 11:52:50', '[管理员: books]在 2018-08-08 11:52:50,操作了: /showManager方法 .');
INSERT INTO `mlog` VALUES ('256', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('257', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('258', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('259', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('260', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('261', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('262', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('263', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('264', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('265', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('266', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('267', 'books', '2018-08-08 11:52:55', '[管理员: books]在 2018-08-08 11:52:55,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('268', 'books', '2018-08-08 11:52:56', '[管理员: books]在 2018-08-08 11:52:56,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('269', 'books', '2018-08-08 11:53:28', '[管理员: books]在 2018-08-08 11:53:28,操作了: /addData方法 .');
INSERT INTO `mlog` VALUES ('270', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('271', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('272', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('273', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('274', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('275', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('276', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('277', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('278', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('279', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('280', 'books', '2018-08-08 11:53:33', '[管理员: books]在 2018-08-08 11:53:33,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('281', 'books', '2018-08-08 11:53:34', '[管理员: books]在 2018-08-08 11:53:34,操作了: /showData方法 .');
INSERT INTO `mlog` VALUES ('282', 'books', '2018-08-08 11:53:34', '[管理员: books]在 2018-08-08 11:53:34,操作了: /showData方法 .');

-- ----------------------------
-- Table structure for `newbooks`
-- ----------------------------
DROP TABLE IF EXISTS `newbooks`;
CREATE TABLE `newbooks` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT NULL,
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `艺术与摄影` (`kindid`),
  CONSTRAINT `新书榜` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newbooks
-- ----------------------------
INSERT INTO `newbooks` VALUES ('1', '斗破苍穹', '2018/6/11', '母鸡', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '4');
INSERT INTO `newbooks` VALUES ('2', '斗破苍穹2', '2018/6/11', '母鸡2', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('3', '斗破苍穹3', '2018/6/11', '母鸡3', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('4', '斗破苍穹4', '2018/6/11', '母鸡4', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('5', '斗破苍穹5', '2018/6/11', '母鸡5', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('6', '斗破苍穹6', '2018/6/11', '母鸡6', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('7', '斗破苍穹7', '2018/6/11', '母鸡7', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('8', '斗破苍穹8', '2018/6/11', '母鸡8', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('9', '斗破苍穹9', '2018/6/11', '母鸡9', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('10', '斗破苍穹10', '2018/6/11', '母鸡10', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('11', '斗破苍穹11', '2018/6/11', '母鸡11', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('12', '斗破苍穹12', '2018/6/11', '母鸡12', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('13', '斗破苍穹13', '2018/6/11', '母鸡13', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('14', '斗破苍穹14', '2018/6/11', '母鸡14', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('15', '斗破苍穹15', '2018/6/11', '母鸡15', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('16', '斗破苍穹16', '2018/6/11', '母鸡16', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('17', '斗破苍穹17', '2018/6/11', '母鸡17', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('18', '斗破苍穹18', '2018/6/11', '母鸡18', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('19', '斗破苍穹19', '2018/6/11', '母鸡19', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('20', '斗破苍穹20', '2018/6/11', '母鸡20', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('21', '斗破苍穹21', '2018/6/11', '母鸡21', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('22', '斗破苍穹22', '2018/6/11', '母鸡22', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('23', '斗破苍穹23', '2018/6/11', '母鸡23', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('24', '斗破苍穹24', '2018/6/11', '母鸡24', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('25', '斗破苍穹25', '2018/6/11', '母鸡25', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('26', '斗破苍穹26', '2018/6/11', '母鸡26', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('27', '斗破苍穹27', '2018/6/11', '母鸡27', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('28', '斗破苍穹28', '2018/6/11', '母鸡28', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('29', '斗破苍穹29', '2018/6/11', '母鸡29', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('30', '斗破苍穹30', '2018/6/11', '母鸡30', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('31', '斗破苍穹31', '2018/6/11', '母鸡31', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('32', '斗破苍穹32', '2018/6/11', '母鸡32', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('33', '斗破苍穹33', '2018/6/11', '母鸡33', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('34', '斗破苍穹34', '2018/6/11', '母鸡34', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('35', '斗破苍穹35', '2018/6/11', '母鸡35', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('36', '斗破苍穹36', '2018/6/11', '母鸡36', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('37', '斗破苍穹37', '2018/6/11', '母鸡37', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('38', '斗破苍穹38', '2018/6/11', '母鸡38', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('39', '斗破苍穹39', '2018/6/11', '母鸡39', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('40', '斗破苍穹40', '2018/6/11', '母鸡40', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('41', '斗破苍穹41', '2018/6/11', '母鸡41', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('42', '斗破苍穹42', '2018/6/11', '母鸡42', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('43', '斗破苍穹43', '2018/6/11', '母鸡43', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('44', '斗破苍穹44', '2018/6/11', '母鸡44', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('45', '斗破苍穹45', '2018/6/11', '母鸡45', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('46', '斗破苍穹46', '2018/6/11', '母鸡46', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('47', '斗破苍穹47', '2018/6/11', '母鸡47', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('48', '斗破苍穹48', '2018/6/11', '母鸡48', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('49', '斗破苍穹49', '2018/6/11', '母鸡49', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('50', '斗破苍穹50', '2018/6/11', '母鸡50', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('51', '斗破苍穹51', '2018/6/11', '母鸡51', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('52', '斗破苍穹52', '2018/6/11', '母鸡52', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', null, 'books', '2');
INSERT INTO `newbooks` VALUES ('53', '斗破苍穹53', '2018/6/11', '母鸡53', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '2');
INSERT INTO `newbooks` VALUES ('54', '斗破苍穹54', '2018/6/11', '母鸡54', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '2');
INSERT INTO `newbooks` VALUES ('55', '斗破苍穹55', '2018/6/11', '母鸡55', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '2');
INSERT INTO `newbooks` VALUES ('56', '斗破苍穹56', '2018/6/11', '母鸡56', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '2');
INSERT INTO `newbooks` VALUES ('57', '斗破苍穹57', '2018/6/11', '母鸡57', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '2');
INSERT INTO `newbooks` VALUES ('58', '斗破苍穹58', '2018/6/11', '母鸡58', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '2');
INSERT INTO `newbooks` VALUES ('59', '斗破苍穹59', '2018/6/11', '母鸡59', 'http://dlc2.pconline.com.cn/filedown_1241187_9887596/IChwFNeD/dpcq_jjb.rar', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '2');
INSERT INTO `newbooks` VALUES ('63', '1111', '111', '1111', '111', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', '111', '2');
INSERT INTO `newbooks` VALUES ('64', '达到ad', '2018-07-25 16:21:55', 'as打算', 'http://120.78.75.213:8080/books/uploads/img/1532507121013免责声明8.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'undefined', '4');
INSERT INTO `newbooks` VALUES ('65', '你好', '2018-07-26 14:12:52', '你还会', 'http://120.78.75.213:8080/books/uploads/img/1532585775515免责声明11.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'undefined', '1');
INSERT INTO `newbooks` VALUES ('66', '大娃娃', '2018-07-26 14:15:50', 'www大多所多是的asdasd', 'http://120.78.75.213:8080/books/uploads/img/1532585954187免责声明13.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'undefined', '2');
INSERT INTO `newbooks` VALUES ('67', 'test1', '2018-08-06 09:21:56', '嘻嘻嘻嘻，动一动一', 'http://120.78.75.213:8080/books/uploads/img/1533518508859免责声明1.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'undefined', '1');
INSERT INTO `newbooks` VALUES ('68', 'test1', '2018-08-06 10:11:46', '嘻嘻嘻is好的骄傲是', 'http://120.78.75.213:8080/books/uploads/img/1533521502125免责声明.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '1');
INSERT INTO `newbooks` VALUES ('69', 'test1', '2018-08-06 14:19:22', 'aaaaa', 'http://120.78.75.213:8080/books/uploads/img/1533536360516免责声明1.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '1');
INSERT INTO `newbooks` VALUES ('70', '1111', '2018-08-08 10:32:41', '1111', 'http://120.78.75.213:8080TimePeoject\\project\\booksBackstage\\src\\main\\webapp\\uploads\\ebooks\\1533695265102距.txt', '1', 'http://120.78.75.213:8080TimePeoject\\project\\booksBackstage\\src\\main\\webapp\\uploads\\imghttp://120.78.75.213:8080/books/uploads/img/12.jpg', 'undefined', '1');
INSERT INTO `newbooks` VALUES ('71', '是的撒', '2018-08-08 10:39:05', '达大厦', 'http://120.78.75.213:8080/books/uploads/ebooks/1533695942810距1.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'undefined', '1');
INSERT INTO `newbooks` VALUES ('72', '12131', '2018-08-08 11:13:05', '121321321', 'http://120.78.75.213:8080/books/uploads/ebooks/1533697981652免责声明3.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'undefined', '1');
INSERT INTO `newbooks` VALUES ('73', '2222', '2018-08-08 11:19:17', '的撒打算的', 'http://120.78.75.213:8080/books/uploads/ebooks/1533698354784免责声明3.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'undefined', '1');
INSERT INTO `newbooks` VALUES ('74', 'ssssss', '2018-08-08 11:53:28', 'sssss', 'http://120.78.75.213:8080/books/uploads/ebooks/1533700405397免责声明3.txt', '1', 'http://120.78.75.213:8080/books/uploads/img/12.jpg', 'books', '1');

-- ----------------------------
-- Table structure for `qk_news`
-- ----------------------------
DROP TABLE IF EXISTS `qk_news`;
CREATE TABLE `qk_news` (
  `newsid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `context` varchar(255) DEFAULT NULL,
  `newstime` varchar(255) DEFAULT NULL,
  `issuer` varchar(255) DEFAULT NULL,
  `titleimg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`newsid`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qk_news
-- ----------------------------
INSERT INTO `qk_news` VALUES ('1', '第一个新闻标题', '嘻嘻嘻哈哈哈哈哈', '2018-07-18 18:33:04', 'books', null);
INSERT INTO `qk_news` VALUES ('2', '第二个新闻标题', '嘻嘻嘻哈哈哈哈哈', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('8', '第8个新闻标题', '嘻嘻嘻哈哈哈哈哈8', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('13', '第13个新闻标题', '嘻嘻嘻哈哈哈哈哈13', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('14', '第14个新闻标题', '嘻嘻嘻哈哈哈哈哈14', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('15', '第15个新闻标题', '嘻嘻嘻哈哈哈哈哈15', '2018-07-18 18:34:52', 'books', null);
INSERT INTO `qk_news` VALUES ('16', '第16个新闻标题', '嘻嘻嘻哈哈哈哈哈16', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('17', '第17个新闻标题', '嘻嘻嘻哈哈哈哈哈17', '2018-07-18 18:37:55', 'books', 'http://120.78.75.213:8080/books/uploads/img/1531910275063timg1.jpg');
INSERT INTO `qk_news` VALUES ('18', '第18个新闻标题', '嘻嘻嘻哈哈哈哈哈18', '2018-07-18 18:36:10', 'books', 'http://120.78.75.213:8080/books/uploads/img/1531910168596timg.jpg');
INSERT INTO `qk_news` VALUES ('19', '第19个新闻标题', '嘻嘻嘻哈哈哈哈哈19', '2018-07-18 18:34:20', 'books', null);
INSERT INTO `qk_news` VALUES ('20', '第20个新闻标题', '嘻嘻嘻哈哈哈哈哈20', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('21', '第21个新闻标题', '嘻嘻嘻哈哈哈哈哈21', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('22', '第22个新闻标题', '嘻嘻嘻哈哈哈哈哈22', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('23', '第23个新闻标题', '嘻嘻嘻哈哈哈哈哈23', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('24', '第24个新闻标题', '嘻嘻嘻哈哈哈哈哈24', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('25', '第25个新闻标题', '嘻嘻嘻哈哈哈哈哈25', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('26', '第26个新闻标题', '嘻嘻嘻哈哈哈哈哈26', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('27', '第27个新闻标题', '嘻嘻嘻哈哈哈哈哈27', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('28', '第28个新闻标题', '嘻嘻嘻哈哈哈哈哈28', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('29', '第29个新闻标题', '嘻嘻嘻哈哈哈哈哈29', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('30', '第30个新闻标题', '嘻嘻嘻哈哈哈哈哈30', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('31', '第31个新闻标题', '嘻嘻嘻哈哈哈哈哈31', '2018-07-19 10:17:33', 'books', 'http://120.78.75.213:8080/books/uploads/img/1531966651612预览图_千图网_编号28551244.jpg');
INSERT INTO `qk_news` VALUES ('32', '第32个新闻标题', '嘻嘻嘻哈哈哈哈哈32', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('33', '第33个新闻标题', '嘻嘻嘻哈哈哈哈哈33', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('34', '第34个新闻标题', '嘻嘻嘻哈哈哈哈哈34', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('35', '第35个新闻标题', '嘻嘻嘻哈哈哈哈哈35', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('36', '第36个新闻标题', '嘻嘻嘻哈哈哈哈哈36', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('37', '第37个新闻标题', '嘻嘻嘻哈哈哈哈哈37', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('38', '第38个新闻标题', '嘻嘻嘻哈哈哈哈哈38', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('39', '第39个新闻标题', '嘻嘻嘻哈哈哈哈哈39', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('40', '第40个新闻标题', '嘻嘻嘻哈哈哈哈哈40', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('41', '第41个新闻标题', '嘻嘻嘻哈哈哈哈哈41', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('42', '第42个新闻标题', '嘻嘻嘻哈哈哈哈哈42', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('43', '第43个新闻标题', '嘻嘻嘻哈哈哈哈哈43', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('44', '第44个新闻标题', '嘻嘻嘻哈哈哈哈哈44', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('45', '第45个新闻标题', '嘻嘻嘻哈哈哈哈哈45', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('46', '第46个新闻标题', '嘻嘻嘻哈哈哈哈哈46', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('47', '第47个新闻标题', '嘻嘻嘻哈哈哈哈哈47', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('48', '第48个新闻标题', '嘻嘻嘻哈哈哈哈哈48', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('49', '第49个新闻标题', '嘻嘻嘻哈哈哈哈哈49', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('50', '第50个新闻标题', '嘻嘻嘻哈哈哈哈哈50', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('51', '第51个新闻标题', '嘻嘻嘻哈哈哈哈哈51', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('52', '第52个新闻标题', '嘻嘻嘻哈哈哈哈哈52', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('53', '第53个新闻标题', '嘻嘻嘻哈哈哈哈哈53', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('54', '第54个新闻标题', '嘻嘻嘻哈哈哈哈哈54', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('55', '第55个新闻标题', '嘻嘻嘻哈哈哈哈哈55', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('56', '第56个新闻标题', '嘻嘻嘻哈哈哈哈哈56', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('57', '第57个新闻标题', '嘻嘻嘻哈哈哈哈哈57', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('58', '第58个新闻标题', '嘻嘻嘻哈哈哈哈哈58', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('59', '第59个新闻标题', '嘻嘻嘻哈哈哈哈哈59', '2018/6/11', 'books', null);
INSERT INTO `qk_news` VALUES ('60', 'sdsadasd', 'awdasdsdasd\nasd\nas\ndasda', '2018-07-19 10:18:25', 'books', 'http://120.78.75.213:8080/books/uploads/img/1531966700848timg.jpg');
INSERT INTO `qk_news` VALUES ('61', 'test1', '这个是第一个测试', '2018-08-07 10:28:59', 'books', 'http://120.78.75.213:8080/books/uploads/img/12.jpg');
INSERT INTO `qk_news` VALUES ('62', 'test2', '这个是第一个测试', '2018-08-07 10:28:59', 'books', 'http://120.78.75.213:8080/books/uploads/img/12.jpg');
INSERT INTO `qk_news` VALUES ('63', 'test3', '这个是第一个测试', '2018-08-07 10:28:59', 'books', 'http://120.78.75.213:8080/books/uploads/img/12.jpg');
INSERT INTO `qk_news` VALUES ('64', 'test4', '这个是第一个测试', '2018-08-07 10:28:59', 'books', 'http://120.78.75.213:8080/books/uploads/img/12.jpg');
INSERT INTO `qk_news` VALUES ('65', 'test5', '这个是第一个测试', '2018-08-07 10:28:59', 'books', 'http://120.78.75.213:8080/books/uploads/img/12.jpg');

-- ----------------------------
-- Table structure for `s_user`
-- ----------------------------
DROP TABLE IF EXISTS `s_user`;
CREATE TABLE `s_user` (
  `date` varchar(255) DEFAULT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是学生成员的表',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL COMMENT '这是学生用户表',
  `nul` int(2) DEFAULT '1',
  `downloadNul` int(255) DEFAULT '0' COMMENT '用户的下载量',
  `shareNul` int(255) DEFAULT '0' COMMENT '用户的分享量，判断是否能成为会员',
  `ifMember` int(255) DEFAULT '0' COMMENT '是否为会员,0为否，1为是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_user
-- ----------------------------
INSERT INTO `s_user` VALUES ('2018-08-06 14:12:45', '1', 'juntao', '123456', '学生1', '1', '2', '0', '0');
INSERT INTO `s_user` VALUES ('2018-07-11 20:20:58', '2', 'lin', '123456', 'xuesheng2', '1', '0', '0', '0');
INSERT INTO `s_user` VALUES ('2018-07-13 09:06:58', '3', 'www对对对', '111111', '8898sdasdasd 1111', '1', '0', '0', '0');
INSERT INTO `s_user` VALUES ('2018-111', '4', 'hello', '1111', '12132132', '1', '38', '0', '1');
INSERT INTO `s_user` VALUES ('2018-08-04 14:45:48', '13', '111', '111', '222', '1', '0', '0', '0');
INSERT INTO `s_user` VALUES ('2018-08-04 15:38:29', '14', 'root', '123456', '11111', '1', '0', '0', '0');
INSERT INTO `s_user` VALUES ('2018-08-04 15:43:40', '15', 'roii', '123456', '1111', '1', '0', '0', '0');

-- ----------------------------
-- Table structure for `share_from_user`
-- ----------------------------
DROP TABLE IF EXISTS `share_from_user`;
CREATE TABLE `share_from_user` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '这是书本的种类',
  `bookname` varchar(255) DEFAULT NULL,
  `altertime` varchar(255) DEFAULT NULL,
  `bookintro` varchar(255) DEFAULT NULL,
  `downloadIVN` varchar(255) DEFAULT NULL,
  `downlaodNUM` varchar(255) DEFAULT NULL,
  `bookimg` varchar(255) DEFAULT NULL,
  `manage` varchar(255) DEFAULT NULL,
  `kindid` int(255) DEFAULT '6',
  `remove` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `社会与科学` (`kindid`),
  CONSTRAINT `share_from_user_ibfk_1` FOREIGN KEY (`kindid`) REFERENCES `kindid` (`kindid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of share_from_user
-- ----------------------------

-- ----------------------------
-- Table structure for `shopping_book`
-- ----------------------------
DROP TABLE IF EXISTS `shopping_book`;
CREATE TABLE `shopping_book` (
  `shopping_id` int(255) NOT NULL AUTO_INCREMENT,
  `shopping_book_id` int(255) DEFAULT NULL,
  `shopping_book_kindid` int(255) DEFAULT NULL,
  `shopping_username` char(255) DEFAULT NULL,
  `shopping_userId` int(255) DEFAULT NULL,
  `shopping_statue` int(255) DEFAULT '0' COMMENT '0代表未下载，1代表已下载',
  PRIMARY KEY (`shopping_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shopping_book
-- ----------------------------
INSERT INTO `shopping_book` VALUES ('1', '73', '1', 'juntao', '1', '1');
INSERT INTO `shopping_book` VALUES ('2', '72', '1', 'juntao', '1', '1');
INSERT INTO `shopping_book` VALUES ('3', '72', '1', 'juntao', '1', '1');
INSERT INTO `shopping_book` VALUES ('4', '71', '1', 'juntao', '1', '1');
INSERT INTO `shopping_book` VALUES ('5', '70', '1', 'juntao', '1', '1');
INSERT INTO `shopping_book` VALUES ('6', '73', '1', 'juntao', '1', '1');
INSERT INTO `shopping_book` VALUES ('7', '72', '1', 'juntao', '1', '1');
INSERT INTO `shopping_book` VALUES ('8', '72', '1', 'juntao', '1', '1');
INSERT INTO `shopping_book` VALUES ('9', '73', '1', 'juntao', '1', '1');

-- ----------------------------
-- Table structure for `ulog`
-- ----------------------------
DROP TABLE IF EXISTS `ulog`;
CREATE TABLE `ulog` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `loguser` varchar(255) DEFAULT NULL,
  `logtime` varchar(255) DEFAULT NULL,
  `logcontent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ulog
-- ----------------------------
INSERT INTO `ulog` VALUES ('3', 'juntao', '2018-07-05 17:27:49', '[普通用户: juntao]在2018-07-05 17:27:49,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('4', 'juntao', '2018-07-05 17:27:49', '[普通用户: juntao]在2018-07-05 17:27:49,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('5', 'juntao', '2018-07-05 17:28:56', '[普通用户: juntao]在2018-07-05 17:28:56,操作了:/showData方法.');
INSERT INTO `ulog` VALUES ('6', 'juntao', '2018-07-05 17:29:03', '[普通用户: juntao]在2018-07-05 17:29:03,操作了:/showData方法.');
INSERT INTO `ulog` VALUES ('7', 'juntao', '2018-07-05 17:29:25', '[普通用户: juntao]在2018-07-05 17:29:25,操作了:/showData方法.');
INSERT INTO `ulog` VALUES ('8', 'juntao', '2018-07-09 10:55:09', '[普通用户: juntao]在2018-07-09 10:55:09,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('9', 'juntao', '2018-07-09 10:55:09', '[普通用户: juntao]在2018-07-09 10:55:09,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('10', 'juntao', '2018-07-09 10:55:18', '[普通用户: juntao]在2018-07-09 10:55:18,操作了:/showData方法.');
INSERT INTO `ulog` VALUES ('11', 'juntao', '2018-07-09 16:01:39', '[普通用户: juntao]在2018-07-09 16:01:39,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('12', 'juntao', '2018-07-09 16:01:39', '[普通用户: juntao]在2018-07-09 16:01:39,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('13', 'juntao', '2018-07-09 16:02:25', '[普通用户: juntao]在2018-07-09 16:02:25,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('14', 'juntao', '2018-07-09 16:02:25', '[普通用户: juntao]在2018-07-09 16:02:25,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('15', 'juntao', '2018-07-09 16:02:25', '[普通用户: juntao]在2018-07-09 16:02:25,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('16', 'juntao', '2018-07-09 16:02:25', '[普通用户: juntao]在2018-07-09 16:02:25,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('17', 'juntao', '2018-07-09 16:02:25', '[普通用户: juntao]在2018-07-09 16:02:25,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('18', 'juntao', '2018-07-09 16:02:25', '[普通用户: juntao]在2018-07-09 16:02:25,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('19', 'juntao', '2018-07-09 16:02:26', '[普通用户: juntao]在2018-07-09 16:02:26,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('20', 'juntao', '2018-07-09 16:02:26', '[普通用户: juntao]在2018-07-09 16:02:26,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('21', 'juntao', '2018-07-09 16:02:26', '[普通用户: juntao]在2018-07-09 16:02:26,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('22', 'juntao', '2018-07-09 16:02:26', '[普通用户: juntao]在2018-07-09 16:02:26,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('23', 'juntao', '2018-07-09 16:02:26', '[普通用户: juntao]在2018-07-09 16:02:26,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('24', 'juntao', '2018-07-09 16:02:26', '[普通用户: juntao]在2018-07-09 16:02:26,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('25', 'juntao', '2018-07-09 16:02:27', '[普通用户: juntao]在2018-07-09 16:02:27,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('26', 'juntao', '2018-07-09 16:02:27', '[普通用户: juntao]在2018-07-09 16:02:27,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('27', 'juntao', '2018-07-09 16:02:27', '[普通用户: juntao]在2018-07-09 16:02:27,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('28', 'juntao', '2018-07-09 16:02:27', '[普通用户: juntao]在2018-07-09 16:02:27,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('29', 'juntao', '2018-07-09 23:37:18', '[普通用户: juntao]在2018-07-09 23:37:18,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('30', 'juntao', '2018-07-09 23:37:18', '[普通用户: juntao]在2018-07-09 23:37:18,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('31', 'juntao', '2018-07-09 23:37:23', '[普通用户: juntao]在2018-07-09 23:37:23,操作了:/findData方法.');
INSERT INTO `ulog` VALUES ('32', 'juntao', '2018-07-09 23:37:33', '[普通用户: juntao]在2018-07-09 23:37:33,操作了:/findData方法.');
INSERT INTO `ulog` VALUES ('33', 'juntao', '2018-07-09 23:37:58', '[普通用户: juntao]在2018-07-09 23:37:58,操作了:/findData方法.');
INSERT INTO `ulog` VALUES ('34', 'juntao', '2018-07-24 15:17:02', '[普通用户: juntao]在2018-07-24 15:17:02,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('35', 'juntao', '2018-07-24 15:17:02', '[普通用户: juntao]在2018-07-24 15:17:02,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('36', 'juntao', '2018-07-24 15:20:46', '[普通用户: juntao]在2018-07-24 15:20:46,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('37', 'juntao', '2018-07-24 15:20:46', '[普通用户: juntao]在2018-07-24 15:20:46,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('38', 'juntao', '2018-07-24 15:23:36', '[普通用户: juntao]在2018-07-24 15:23:36,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('39', 'juntao', '2018-07-24 15:23:50', '[普通用户: juntao]在2018-07-24 15:23:50,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('40', 'juntao', '2018-07-24 15:23:51', '[普通用户: juntao]在2018-07-24 15:23:51,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('41', 'juntao', '2018-07-24 15:26:22', '[普通用户: juntao]在2018-07-24 15:26:22,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('42', 'juntao', '2018-07-24 15:26:22', '[普通用户: juntao]在2018-07-24 15:26:22,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('43', 'juntao', '2018-07-24 15:37:10', '[普通用户: juntao]在2018-07-24 15:37:10,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('44', 'juntao', '2018-07-24 15:37:10', '[普通用户: juntao]在2018-07-24 15:37:10,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('45', 'juntao', '2018-07-24 15:42:35', '[普通用户: juntao]在2018-07-24 15:42:35,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('46', 'juntao', '2018-07-24 15:42:49', '[普通用户: juntao]在2018-07-24 15:42:49,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('47', 'juntao', '2018-07-24 15:42:49', '[普通用户: juntao]在2018-07-24 15:42:49,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('48', 'juntao', '2018-07-24 15:43:10', '[普通用户: juntao]在2018-07-24 15:43:10,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('49', 'juntao', '2018-07-24 15:43:25', '[普通用户: juntao]在2018-07-24 15:43:25,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('50', 'juntao', '2018-07-24 15:43:25', '[普通用户: juntao]在2018-07-24 15:43:25,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('51', 'juntao', '2018-07-24 15:43:54', '[普通用户: juntao]在2018-07-24 15:43:54,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('52', 'juntao', '2018-07-24 15:43:54', '[普通用户: juntao]在2018-07-24 15:43:54,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('53', 'juntao', '2018-07-24 15:53:58', '[普通用户: juntao]在2018-07-24 15:53:58,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('54', 'juntao', '2018-07-24 15:53:58', '[普通用户: juntao]在2018-07-24 15:53:58,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('55', 'juntao', '2018-07-24 15:57:16', '[普通用户: juntao]在2018-07-24 15:57:16,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('56', 'juntao', '2018-07-24 15:57:31', '[普通用户: juntao]在2018-07-24 15:57:31,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('57', 'juntao', '2018-07-24 15:57:32', '[普通用户: juntao]在2018-07-24 15:57:32,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('58', 'juntao', '2018-07-24 16:05:45', '[普通用户: juntao]在2018-07-24 16:05:45,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('59', 'lin', '2018-07-24 16:06:01', '[普通用户: lin]在2018-07-24 16:06:01,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('60', 'lin', '2018-07-24 16:06:01', '[普通用户: lin]在2018-07-24 16:06:01,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('61', 'juntao', '2018-07-24 16:06:36', '[普通用户: juntao]在2018-07-24 16:06:36,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('62', 'juntao', '2018-07-24 16:06:36', '[普通用户: juntao]在2018-07-24 16:06:36,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('63', 'juntao', '2018-07-24 16:25:25', '[普通用户: juntao]在2018-07-24 16:25:25,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('64', 'juntao', '2018-07-24 16:25:25', '[普通用户: juntao]在2018-07-24 16:25:25,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('65', 'juntao', '2018-07-25 16:02:51', '[普通用户: juntao]在2018-07-25 16:02:51,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('66', 'juntao', '2018-07-25 16:02:51', '[普通用户: juntao]在2018-07-25 16:02:51,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('67', 'juntao', '2018-08-01 11:04:14', '[普通用户: juntao]在2018-08-01 11:04:14,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('68', 'juntao', '2018-08-01 11:04:14', '[普通用户: juntao]在2018-08-01 11:04:14,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('69', 'juntao', '2018-08-03 10:28:39', '[普通用户: juntao]在2018-08-03 10:28:39,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('70', 'juntao', '2018-08-03 10:28:40', '[普通用户: juntao]在2018-08-03 10:28:40,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('71', 'juntao', '2018-08-03 10:36:16', '[普通用户: juntao]在2018-08-03 10:36:16,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('72', 'juntao', '2018-08-03 10:36:16', '[普通用户: juntao]在2018-08-03 10:36:16,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('73', 'juntao', '2018-08-03 13:33:55', '[普通用户: juntao]在2018-08-03 13:33:55,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('74', 'juntao', '2018-08-03 13:33:55', '[普通用户: juntao]在2018-08-03 13:33:55,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('75', 'juntao', '2018-08-04 12:52:31', '[普通用户: juntao]在2018-08-04 12:52:31,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('76', 'juntao', '2018-08-04 12:52:31', '[普通用户: juntao]在2018-08-04 12:52:31,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('77', 'juntao', '2018-08-04 13:40:05', '[普通用户: juntao]在2018-08-04 13:40:05,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('78', 'juntao', '2018-08-04 15:14:21', '[普通用户: juntao]在2018-08-04 15:14:21,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('79', 'juntao', '2018-08-04 15:14:21', '[普通用户: juntao]在2018-08-04 15:14:21,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('80', 'juntao', '2018-08-04 15:17:42', '[普通用户: juntao]在2018-08-04 15:17:42,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('81', 'juntao', '2018-08-04 15:17:55', '[普通用户: juntao]在2018-08-04 15:17:55,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('82', 'juntao', '2018-08-04 15:18:17', '[普通用户: juntao]在2018-08-04 15:18:17,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('83', 'juntao', '2018-08-04 15:18:17', '[普通用户: juntao]在2018-08-04 15:18:17,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('84', 'juntao', '2018-08-04 15:18:36', '[普通用户: juntao]在2018-08-04 15:18:36,操作了:/Member方法.');
INSERT INTO `ulog` VALUES ('85', 'juntao', '2018-08-04 15:20:50', '[普通用户: juntao]在2018-08-04 15:20:50,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('86', 'juntao', '2018-08-04 15:20:50', '[普通用户: juntao]在2018-08-04 15:20:50,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('87', 'juntao', '2018-08-04 15:23:11', '[普通用户: juntao]在2018-08-04 15:23:11,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('88', 'juntao', '2018-08-04 15:23:11', '[普通用户: juntao]在2018-08-04 15:23:11,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('89', 'juntao', '2018-08-04 15:23:18', '[普通用户: juntao]在2018-08-04 15:23:18,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('90', 'juntao', '2018-08-04 15:23:28', '[普通用户: juntao]在2018-08-04 15:23:28,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('91', 'juntao', '2018-08-04 15:23:32', '[普通用户: juntao]在2018-08-04 15:23:32,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('92', 'juntao', '2018-08-04 15:23:45', '[普通用户: juntao]在2018-08-04 15:23:45,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('93', 'juntao', '2018-08-04 15:23:52', '[普通用户: juntao]在2018-08-04 15:23:52,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('94', 'juntao', '2018-08-04 15:27:51', '[普通用户: juntao]在2018-08-04 15:27:51,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('95', 'juntao', '2018-08-04 15:27:51', '[普通用户: juntao]在2018-08-04 15:27:51,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('96', 'juntao', '2018-08-04 15:28:00', '[普通用户: juntao]在2018-08-04 15:28:00,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('97', 'juntao', '2018-08-04 18:37:17', '[普通用户: juntao]在2018-08-04 18:37:17,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('98', 'juntao', '2018-08-04 18:37:17', '[普通用户: juntao]在2018-08-04 18:37:17,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('99', 'juntao', '2018-09-20 08:59:28', '[普通用户: juntao]在2018-09-20 08:59:28,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('100', 'juntao', '2018-09-20 09:10:09', '[普通用户: juntao]在2018-09-20 09:10:09,操作了:/login方法.');
INSERT INTO `ulog` VALUES ('101', 'juntao', '2018-09-20 09:10:09', '[普通用户: juntao]在2018-09-20 09:10:09,操作了:/showUser方法.');
INSERT INTO `ulog` VALUES ('102', 'juntao', '2018-09-20 09:10:10', '[普通用户: juntao]在2018-09-20 09:10:10,操作了:/newsLink方法.');
INSERT INTO `ulog` VALUES ('103', 'juntao', '2018-09-20 09:10:10', '[普通用户: juntao]在2018-09-20 09:10:10,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('104', 'juntao', '2018-09-20 09:10:10', '[普通用户: juntao]在2018-09-20 09:10:10,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('105', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('106', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('107', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('108', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('109', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('110', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('111', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('112', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('113', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('114', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('115', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/allBooksLink方法.');
INSERT INTO `ulog` VALUES ('116', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/newBooksLink方法.');
INSERT INTO `ulog` VALUES ('117', 'juntao', '2018-09-20 09:10:11', '[普通用户: juntao]在2018-09-20 09:10:11,操作了:/newsLink方法.');
INSERT INTO `ulog` VALUES ('118', 'juntao', '2018-09-20 09:10:12', '[普通用户: juntao]在2018-09-20 09:10:12,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('119', 'juntao', '2018-09-20 09:10:18', '[普通用户: juntao]在2018-09-20 09:10:18,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('120', 'juntao', '2018-09-20 09:10:18', '[普通用户: juntao]在2018-09-20 09:10:18,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('121', 'juntao', '2018-09-20 09:10:25', '[普通用户: juntao]在2018-09-20 09:10:25,操作了:/showData方法.');
INSERT INTO `ulog` VALUES ('122', 'juntao', '2018-09-20 09:10:26', '[普通用户: juntao]在2018-09-20 09:10:26,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('123', 'juntao', '2018-09-20 09:10:28', '[普通用户: juntao]在2018-09-20 09:10:28,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('124', 'juntao', '2018-09-20 09:10:53', '[普通用户: juntao]在2018-09-20 09:10:53,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('125', 'juntao', '2018-09-20 09:10:53', '[普通用户: juntao]在2018-09-20 09:10:53,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('126', 'juntao', '2018-09-20 09:11:01', '[普通用户: juntao]在2018-09-20 09:11:01,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('127', 'juntao', '2018-09-20 09:11:26', '[普通用户: juntao]在2018-09-20 09:11:26,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('128', 'juntao', '2018-09-20 09:12:35', '[普通用户: juntao]在2018-09-20 09:12:35,操作了:/showData方法.');
INSERT INTO `ulog` VALUES ('129', 'juntao', '2018-09-20 09:12:35', '[普通用户: juntao]在2018-09-20 09:12:35,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('130', 'juntao', '2018-09-20 09:12:37', '[普通用户: juntao]在2018-09-20 09:12:37,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('131', 'juntao', '2018-09-20 09:12:40', '[普通用户: juntao]在2018-09-20 09:12:40,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('132', 'juntao', '2018-09-20 09:12:43', '[普通用户: juntao]在2018-09-20 09:12:43,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('133', 'juntao', '2018-09-20 09:12:44', '[普通用户: juntao]在2018-09-20 09:12:44,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('134', 'juntao', '2018-09-20 09:12:48', '[普通用户: juntao]在2018-09-20 09:12:48,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('135', 'juntao', '2018-09-20 09:12:48', '[普通用户: juntao]在2018-09-20 09:12:48,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('136', 'juntao', '2018-09-20 09:12:49', '[普通用户: juntao]在2018-09-20 09:12:49,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('137', 'juntao', '2018-09-20 09:12:50', '[普通用户: juntao]在2018-09-20 09:12:50,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('138', 'juntao', '2018-09-20 09:12:53', '[普通用户: juntao]在2018-09-20 09:12:53,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('139', 'juntao', '2018-09-20 09:12:54', '[普通用户: juntao]在2018-09-20 09:12:54,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('140', 'juntao', '2018-09-20 09:12:56', '[普通用户: juntao]在2018-09-20 09:12:56,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('141', 'juntao', '2018-09-20 09:12:58', '[普通用户: juntao]在2018-09-20 09:12:58,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('142', 'juntao', '2018-09-20 09:12:59', '[普通用户: juntao]在2018-09-20 09:12:59,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('143', 'juntao', '2018-09-20 09:13:11', '[普通用户: juntao]在2018-09-20 09:13:11,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('144', 'juntao', '2018-09-20 09:13:13', '[普通用户: juntao]在2018-09-20 09:13:13,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('145', 'juntao', '2018-09-20 09:13:17', '[普通用户: juntao]在2018-09-20 09:13:17,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('146', 'juntao', '2018-09-20 09:13:17', '[普通用户: juntao]在2018-09-20 09:13:17,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('147', 'juntao', '2018-09-20 09:13:21', '[普通用户: juntao]在2018-09-20 09:13:21,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('148', 'juntao', '2018-09-20 09:13:21', '[普通用户: juntao]在2018-09-20 09:13:21,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('149', 'juntao', '2018-09-20 09:13:21', '[普通用户: juntao]在2018-09-20 09:13:21,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('150', 'juntao', '2018-09-20 09:13:21', '[普通用户: juntao]在2018-09-20 09:13:21,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('151', 'juntao', '2018-09-20 09:13:21', '[普通用户: juntao]在2018-09-20 09:13:21,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('152', 'juntao', '2018-09-20 09:13:22', '[普通用户: juntao]在2018-09-20 09:13:22,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('153', 'juntao', '2018-09-20 09:13:22', '[普通用户: juntao]在2018-09-20 09:13:22,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('154', 'juntao', '2018-09-20 09:13:24', '[普通用户: juntao]在2018-09-20 09:13:24,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('155', 'juntao', '2018-09-20 09:13:26', '[普通用户: juntao]在2018-09-20 09:13:26,操作了:/shopping_add方法.');
INSERT INTO `ulog` VALUES ('156', 'juntao', '2018-09-20 09:13:29', '[普通用户: juntao]在2018-09-20 09:13:29,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('157', 'juntao', '2018-09-20 09:13:29', '[普通用户: juntao]在2018-09-20 09:13:29,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('158', 'juntao', '2018-09-20 09:14:24', '[普通用户: juntao]在2018-09-20 09:14:24,操作了:/showData方法.');
INSERT INTO `ulog` VALUES ('159', 'juntao', '2018-09-20 09:14:25', '[普通用户: juntao]在2018-09-20 09:14:25,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('160', 'juntao', '2018-09-20 09:14:25', '[普通用户: juntao]在2018-09-20 09:14:25,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('161', 'juntao', '2018-09-20 09:14:26', '[普通用户: juntao]在2018-09-20 09:14:26,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('162', 'juntao', '2018-09-20 09:14:38', '[普通用户: juntao]在2018-09-20 09:14:38,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('163', 'juntao', '2018-09-20 09:14:38', '[普通用户: juntao]在2018-09-20 09:14:38,操作了:/shopping_del方法.');
INSERT INTO `ulog` VALUES ('164', 'juntao', '2018-09-20 09:14:40', '[普通用户: juntao]在2018-09-20 09:14:40,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('165', 'juntao', '2018-09-20 09:14:41', '[普通用户: juntao]在2018-09-20 09:14:41,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('166', 'juntao', '2018-09-20 09:14:48', '[普通用户: juntao]在2018-09-20 09:14:48,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('167', 'juntao', '2018-09-20 09:14:49', '[普通用户: juntao]在2018-09-20 09:14:49,操作了:/memberIf方法.');
INSERT INTO `ulog` VALUES ('168', 'juntao', '2018-09-20 09:15:24', '[普通用户: juntao]在2018-09-20 09:15:24,操作了:/shoppingData方法.');
INSERT INTO `ulog` VALUES ('169', 'juntao', '2018-09-20 09:15:24', '[普通用户: juntao]在2018-09-20 09:15:24,操作了:/memberIf方法.');
